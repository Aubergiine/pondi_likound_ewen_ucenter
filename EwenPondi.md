# U-Center
![](/uploads/0b3cbb44d1ad13cd32b3ef96003ed8df/image.png)

## Table of contents

1. Introduction
2. Repository's description
3. Pull Request

## Introduction

   U-center is a free software that comes straight from U-blox, a company that has been creating semiconductors and wireless modules since 1997 for the consumer, automotive and industrial markets. I had the opportunity to use one of their GPS during a project in the 2nd year of my DUT GEII and that's where I got to know U-center, the software not only allows us to configure the GPS but it's also very usefull to capture and record the data that the GPS receives and U-center makes it very easy to analyze this data even if it can be a bit overwhelming at first :blush:

## Repository's description


**A little more information about U-center Repository**

| Number of contributors | Commits | Number of external documentation | Branch | Forks | Stars | Ver.|
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 13 | 460 | 18 | 2 | 19 | 82 | v0.10.1 |

### Languages :notebook:

- C *87.4%*
- Python *10.1%*
- Assembly *0.7%*
- CMake *0.7%*
- Makefile *0.5%*
- C++ *0.2%*
- Other *0.4%*

## Pull Request

Since the last month, 19 problems have been solved but there are still 18 before the project will be operational.
Everyone can do a pull request but there are a few rules to respect or else, your pull will be erased. First you need to have a clear and understandable title for your pull request, secondly you only can write in english, it make easier for everyone to understand eachother and finaly, only sources in english are allowed for the same reasons.

## External documentation

Click [here](https://www.u-blox.com/en/product/u-center) if you want to know more in details about how U-Center works or, click on the file folder if you want to know who is the U-blox company   [:file_folder:](https://www.u-blox.com/en).

